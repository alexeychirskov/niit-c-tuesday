/*�������� ���������, ������� ������ ������� ������������� ���-
����� ��� ������������� �����, ��� �������� ������� � ������-
��� ������. ��������� ������ �������� �� ����� ������� �����-
��������, ��������������� �� �������� �������*/
#include <stdlib.h>
#include <stdio.h>
typedef struct FREQ TFREQ;
typedef TFREQ * PFREQ;
#define N 256
struct FREQ
{
	char symbol;
	float freq;
} *p;
int comp(const void *a, const void *b)
{
	return (*(PFREQ)b).freq - (*(PFREQ)a).freq;
}
int main(int argc, char *argv[])
{
	FILE *fp;
	int c,i=0,count=0;
	if(argc>=3){
		printf("Too many options\n");
		exit(1);
	}
	else if(argc<2){
		printf("Need more options\n");
		exit(2);
	}
	fp=fopen(argv[1], "rt");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	p=(PFREQ)calloc(N, sizeof(TFREQ));
	if(p==NULL)
		exit(1);
	while ((c=fgetc(fp))!=EOF)
    {
		p[c].symbol=c;
        p[c].freq++;
        count++;
    }
	qsort(p,N,sizeof(TFREQ),comp);
	for(i=0;i<N;i++)
	{
		p[i].freq=p[i].freq/count;
		if(p[i].freq!=0)
		{
			printf("%c",p[i].symbol);
			printf(" - %f\n",p[i].freq);
		}
	}
	free(p);
	return 0;
}