#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define N 100

struct COUNTRY
{
    char iso[2];
    int code;        
    char name[N];
};

typedef struct COUNTRY TCOUNTRY;
typedef TCOUNTRY * PCOUNTRY;

struct ITEM
{
    PCOUNTRY country;
	struct ITEM* next;
	struct ITEM* prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;

PITEM createList(PCOUNTRY country);
PCOUNTRY createName(char *line);
PITEM addToTail(PITEM tail, PCOUNTRY country);
int countList(PITEM head);
PITEM findByName(PITEM head, char *name);
PITEM findByISO(PITEM head, char *iso);
void printName(PITEM item);
void printISO(PITEM item);