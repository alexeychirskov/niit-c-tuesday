#include "Task2.h"
void chomp(char *pstr)
{
	if(pstr[strlen(pstr)-1]=='\n')
		pstr[strlen(pstr)-1]=0;
}
PNODE AddTree(PNODE node, char *pstr)
{
	int i=0;
	if(node==NULL)
	{
		node=(PNODE)malloc(sizeof(struct NODE));
		node->count=0;
		node->left=NULL;
		node->right=NULL;
		while(pstr[i])
		{
			node->symbol[i]=pstr[i];
			i++;
		}
		node->symbol[i]=0;
		return node;
	}
	else if (strcmp(node->symbol,pstr)>0)
		node->left=AddTree(node->left,pstr);
	else if (strcmp(node->symbol,pstr)<0)
		node->right=AddTree(node->right,pstr);
	return node;
}
PNODE SearchTree(PNODE node, char *pstr)
{
	if(strcmp(node->symbol,pstr)==0)
	{
        node->count++;
        return node;
    }
	if(strcmp(node->symbol,pstr)!=0)
	{
		if(node->right==NULL)
			return NULL;
		else
			return SearchTree(node->right,pstr);
	}
	else
	{
		if(node->left==NULL)
			return NULL;
		else return SearchTree(node->left,pstr);
	}
}
void PrintTree(PNODE node)
{
	if(node->left)
		PrintTree(node->left);
	if(node->count>0)
		printf("%s - %d\n",node->symbol,node->count);
	if(node->right)
		PrintTree(node->right);
}