/*�������� ���������, ��������� ������ �� ������ ��������. ������� ���-
������ ������� � ������ ������, � ����� ������ � ������� ����� �������,
���� �� ���������� ������ 1.
���������:
� ������ ��������� ��������� ��������� �������������� �������, �� ����
���������� ���������� � �������� ������. ����� ���������� ��������� ���-
����� �� �����.

������� 1. ������ ������ ������������ �������.
*/
#include <stdio.h>
#include <string.h>
#define N 80//����� ������
int main()
{
	char str[N];
	int i,lastIndex=-1;
	puts("Enter a line:");
	fgets(str,N,stdin);
	for(i=0;i<strlen(str)-1;i++)
	{ 
		for(i;i<strlen(str)-1;i++)
		{
			if (str[i]!=' ' && str[i+1]!=' ')
				putchar(str[i]);
			else if (str[i]!=' ' && str[i+1]==' ')
			{
				putchar(str[i]);
				putchar(' ');
			}
		}
		i=0;//�������� ����� ������
		while(str[i]!='\n' && str[i]!='\0')
		{
			lastIndex++;
			i++;
		}
		str[lastIndex+2]='\0';
	}
	printf("\n");
	return 0;
}