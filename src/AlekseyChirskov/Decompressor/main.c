#include "decompressor.h"

int main(int argc, char **argv)
{
	FILE *fp,*bin_file,*fp_rar;
	char buf[8]={0},name[50]={0};
	int i,count,ch;
	long length, new_length;
	PSYM node=NULL;
	PSYM syms=(PSYM)calloc(M,sizeof(TSYM));
	PSYM *psyms=(PSYM*)calloc(M,sizeof(PSYM));
	if(argc>=4){
		printf("Too many options!");
		exit(1);
	}
	else if(argc<3){
		printf("Need more options!");
		exit(2);
	}
	fp_rar=fopen(argv[1], "rb");
	bin_file=fopen("temp.txt", "w+b");
	fseek(fp_rar,3,SEEK_SET);
	fread(&count,sizeof(int),1,fp_rar);
	fread(&length,sizeof(long),1,fp_rar);
	fread(buf, sizeof(char), 5, fp_rar);
	strcpy(name,argv[2]);
	strcat(name,".");
	strcat(name,buf);
	fp=fopen(name, "wb");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	for(i=0;i<count;i++)
	{
		fread(&syms[i].ch,sizeof(syms[i].ch),1,fp_rar);
		fread(&syms[i].freq,sizeof(syms[i].freq),1,fp_rar);
		psyms[i]=&syms[i];
    }
	node=buildTree(psyms, count);
	makeCodes(node);
	while(fread(buf,sizeof(char),1,fp_rar))
    {
        pack(buf);
        fwrite(buf,sizeof(char),8,bin_file);
    }
    rewind(bin_file);
	for(i=0;i<length;i++)
        comeback(bin_file,fp,node);
	new_length=getFileLength(fp);
	if(length!=new_length)
	{
		printf("Wrong size of file!");
		exit(3);
	}
	fcloseall();
	remove("temp.txt"); 
	free(syms);
	free(psyms);
	return 0;
}