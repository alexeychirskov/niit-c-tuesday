/*�������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1024//����� ������
int WordCount(char *str, int size)//������� ��� �������� ���� � ������
{
	int i,count=0,inword=0;
	for(i=0;i<size;i++)
	{
		if (*(str+i)==' ' || *(str+i)=='\n' || *(str+i)=='\t')
			inword=0;
		else if (inword==0)
		{
			inword=1;
			count++;
		}	
	}
	return count;
}
void getWord(char *str, char **pstr, int size)//��������� ������ ���������� �������� ������ ���� ����
{
	int i,j=0,inword=0;
	for(i=0;i<size;i++)
	{
		if (*(str+i)==' ' || *(str+i)=='\n' || *(str+i)=='\t')
			inword=0;
		else if (inword==0)
		{
			inword=1;
			pstr[j]=&str[i];
			j++;
		}
	}	
}
void printWord(char *pstr)//������� ����� � ������ ��� ����� �������(����� ������� � ����������)
{
	int i=0,j,count;
	char tmp;
	srand(time(NULL));
	while(pstr[i]!=' ' && pstr[i]!='\0' && pstr[i]!='\t' && pstr[i]!='\n')
		i++;
	putchar(pstr[0]);
	for(j=1;j<i-1;j++)
	{
		count=rand()%(i-1-j)+j;
		tmp=pstr[count];
		pstr[count]=pstr[j];
		pstr[j]=tmp;
		putchar(pstr[j]);
	}
	putchar(pstr[i-1]);
}
int main()
{
	int i,size,count;
	char str[N],**pstr;
	FILE *fp;
	fp=fopen("input.txt","rt");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	while(fgets(str,sizeof(str),fp))//��������� ������ ���������
	{
		size=strlen(str);//size ���-�� ��������� � ������
		count=WordCount(str,size);//count ���-�� ���� � ������
		pstr=(char**)malloc(count*sizeof(char*));//������ ����������
		if(pstr == NULL) 
		{
			printf("Error!\n");
			exit(1);
		}
		getWord(str,pstr,size);
		for(i=0;i<count;i++)
		{
			printWord(pstr[i]);
			printf(" ");
		}
	}
	fclose(fp);
	return 0;
}