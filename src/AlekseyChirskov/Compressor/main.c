#include "compressor.h"

int main(int argc, char **argv)
{
	int count,i=0,ch;
	FILE *fp,*bin_file,*fp_rar;
	long length;
	UC buf[8];
	char end[5]={0}, name[50]={0}, *pend, *pname;
	PSYM node=NULL;
	PSYM syms=(PSYM)calloc(M,sizeof(TSYM));
	PSYM *psyms=(PSYM*)calloc(M,sizeof(PSYM));
	if(argc>=3){
		printf("Too many options!");
		exit(1);
	}
	else if(argc<2){
		printf("Need more options!");
		exit(2);
	}
	fp=fopen(argv[1], "rb");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	pend=end;
	pname=name;
	i=fileExtension(argv,pend);
	fileName(&i,argv,pname);
	strcat(name,".myrar"); 
	freq(fp,syms);
	count=uniqueSYMcount(syms);
	if(count==1)
	{
		puts("Wrong file format!");
		exit(3);
	}
	for(i=0;i<count;i++)
		psyms[i]=&syms[i];
	node=buildTree(psyms,count);
	makeCodes(node);
	rewind(fp);
	bin_file=fopen("temp.txt","w+b");
	while((ch=fgetc(fp))!=EOF)
	{
		for(i=0;i<count;i++)
			if(ch==syms[i].ch)
			{
				fputs(syms[i].code,bin_file);
				break;
			}
	}
	fp_rar=fopen(name,"wb");
	fwrite("ASC",sizeof(char),3,fp_rar);//�������� �������
	fwrite(&count,sizeof(int),1,fp_rar);//���-�� ���������� ��������
	length=getFileLength(fp);
	fwrite(&length,sizeof(long),1,fp_rar);//������ ��������� �����
	fwrite(pend,sizeof(char),5,fp_rar);//�������� ���������� �����
	for(i=0;i<count;i++)//������� �������������
	{
		fwrite(&syms[i].ch,sizeof(UC),1,fp_rar);
		fwrite(&syms[i].freq,sizeof(UI),1,fp_rar);
	}
	rewind(bin_file);
	while(fread(buf,sizeof(char),8,bin_file))
		fputc(pack(buf),fp_rar);
	fcloseall();
	remove("temp.txt");
	free(syms);
	free(psyms);
	return 0;
}