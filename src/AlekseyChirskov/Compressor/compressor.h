#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define M 256

typedef unsigned char UC;
typedef unsigned int UI;
typedef unsigned short US;

struct SYM//������������� �������
{
	UC ch;//ASCII-���
	UI freq;//������� �������������
	char code[256];//������ ��� ������ ����
	struct SYM *left;//����� ������� � ������
	struct SYM *right;//������ ������� � ������
};
typedef struct SYM TSYM;
typedef TSYM * PSYM;

union CODE {
	UC ch;
	struct {
		US b1:1;
		US b2:1;
		US b3:1;
		US b4:1;
		US b5:1;
		US b6:1;
		US b7:1;
		US b8:1;
	} byte;
};
void freq(FILE *fp, PSYM syms);//������� �������������(������������ � ����������)
void makeCodes(PSYM node);
PSYM buildTree(PSYM *psym, int N);
int uniqueSYMcount(PSYM syms);//���-�� ���������� ��������
long getFileLength(FILE *file);//����� ��������� �����
int comp(const void *a, const void *b);
UC pack(UC *buf);
void coup(char *str, int i);//���������� ����������� �������
void swap(char str, int a, int b);//�����
int	fileExtension(char **argv, char *pend);//���������� �����
void fileName(int *number, char **argv, char *pname);//�������� �����