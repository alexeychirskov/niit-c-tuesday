/*�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������*/
#include <stdio.h>
#include <string.h>
#define N 80//����� ������
void clean_stdin()
{
	int c;
	do {
		c=getchar();
	}while(c!='\n' && c !=EOF);
}
int main() 
{
	char str[N]={0};
	int c,i=0,j=-1,count=0,inword=0,word_number,word_lenght,word_head=0,flag=0,new_flag=0;
	printf("Enter the number: \n");
	do {
		if(scanf("%d",&word_number)<1)
		{
			printf("Error, please try again!\n");
			clean_stdin();
		}
		else new_flag=1;
	}while(new_flag==0);
	puts("Enter the line: ");
	fgets(str,N,stdin);
	while((c=getchar()) != '\n')
	{
		str[i]=c;
		i++;
		j++;
		if (c==' ' || c=='\n' || c=='\t')
			inword=0;
		else if (inword==0)
		{
			inword=1;
			count++;
			if(word_number==count)
			{
				word_head=j;
				j=0;
			}
			if((word_number+1)==count)
			{
				word_lenght=j;
				flag=1;
			}
		}
	}
	if(flag==0)
		word_lenght=j+1;
	if(word_number>count)
		printf("Word with this number doesn't exist!\n");
	while(word_lenght)
	{
		for(i=word_lenght+word_head-1;i<strlen(str);i++)
			str[i]=str[i+1];
		word_lenght--;
	}
	printf("%s\n",str);
	return 0;
}