/* �������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ������-
��� ������. ������������� ��������� 4-� �������� ��������. ��-
����� ���������� ������������ �������� ��������*/
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <math.h>
#define N 50
char *prog;//������������� ���������
char token[N];
char token_type;
void eval_exp (double *answer);
void eval_exp2(double *answer);
void eval_exp3(double *answer);
void eval_exp4(double *answer);
void eval_exp5(double *answer);
int isdelim(char c)
{
	if(strchr("+-/*()", c) || c==9 || c=='\r' || c==0)
		return 1;
	return 0;
}
void get_token(void)
{
	char *temp;
	token_type=0;
	temp=token;
	if(!*prog) return;
	if(strchr("+-*/()", *prog))
	{
		token_type=1;
		*temp++ = *prog++;
	}
	else if(isdigit(*prog))
	{
		while(!isdelim(*prog))
			*temp++=*prog++;
		token_type=2;
	}
	*temp = '\0';
}
void serror(int error)//����������� ��������� �� ������
{
	static char *err[]={
		"�������������� ������",
		"������������������ ������",
		"��� ���������",
		"������� �� ����"
	};
	printf("%s\n", err[error]);
	exit(1);
}
void atom(double *answer)//��������� �������� � �������
{
	if(token_type==2)
	{
		*answer=atof(token);
		get_token();
		return;
	}
	serror(0);
}
void putback(void)
{
	char *p;
	p = token;
	for(;*p;p++)
		prog--;
}
void eval_exp(double *answer)//����
{
	get_token();
	if(!*token)
	{
		serror(2);
		return;
	}
	eval_exp2(answer);
}
void eval_exp2(double *answer)//�������� ��� ���������
{
	char op;
	double temp;
	eval_exp3(answer);
	while((op = *token) == '+' || op == '-')
	{
		get_token();
		eval_exp3(&temp);
		switch(op)
		{
			case '-':
				*answer = *answer - temp;
				break;
			case '+':
				*answer = *answer + temp;
				break;
		}
	}
}
void eval_exp3(double *answer)//��������� ��� �������
{
	char op;
	double temp;
	eval_exp4(answer);
	while((op = *token) == '*' || op == '/' || op == '%')
	{
		get_token();
		eval_exp4(&temp);
		switch(op)
		{
			case '*':
				*answer = *answer * temp;
				break;
			case '/':
				if(temp == 0.0)
				{
					serror(3);//������� �� ����
					*answer = 0.0;
				}
				else *answer = *answer / temp;
				break;
    
		}
	}
}

void eval_exp4(double *answer)//��������� ������� ����������
{
	char op=0;
	if((token_type==1) && *token=='+' || *token == '-')
	{
		op = *token;
		get_token();
	}
	eval_exp5(answer);
	if(op == '-') *answer = -(*answer);
}
void eval_exp5(double *answer)//���������� ��������� � �������
{
	if(*token == '(')
	{
		get_token();
		eval_exp2(answer);
		if(*token != ')')
			serror(1);
		get_token();
	}
	else
		atom(answer);
}
int main(int argc, char** argv)
{
	double answer,number;
	int i=0;
	if(argc>=3)
	{
		printf("Too many options\n");
		exit(1);
	}
	else if(argc<2)
	{
		printf("Need more options\n");
		exit(2);
	}
	else if(argc==2)
	{
		while(argv[1][i])
		i++;
	}
	setlocale(LC_ALL,"RUS");
	prog=&argv[1][0];
	eval_exp(&answer);
	number=modf(answer, &number);
	if(number==0)
		printf("���������: %.0f\n", answer);
	else
		printf("���������: %.2f\n", answer);
	return 0;
}