/*�������� ���������, ������� ��������� ������ ������������ �
����������� ���������*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define CLOCKS_PER_SEC 1000
typedef unsigned long UL;
#define N 2//�������
int sum_new(UL *p, UL size)
{
	if(size==1)
		return *p;
	else
		return sum_new(p,size/2)+sum_new(p+size/2,size-size/2);
}
int sum_old(UL *p, UL size)
{
	UL i,sum=0;
	for(i=0;i<size;i++)
		sum+=p[i];
	return sum;
}
int power(int x, int n)
{
	if(n==0)
		return 1;
	else
		return x*power(x,n-1);
}
void random(UL *p, UL size)
{
	UL i;
	srand(time(NULL));
	for(i=0;i<size;i++)
		p[i]=rand()%9+1;
}
int main(int argc, char **argv)
{
	UL i,count,sum_trad,sum_rec,*p,size;
	clock_t t1,t2;
	char a;
	if(argc>=3){
    printf("Too many options, need only number of power\n");
    exit(1);
	}
	else if(argc<2){
		printf("Need more options\n");
		exit(2);
	}
	else if(argc==2)
		count=atol(argv[1]);
	size=power(N,count);
	p=(UL*)calloc(size,sizeof(UL));
	if(!p){
    printf("Error\n");
    exit(1);
	}
	random(p,size);
	t1=clock();
	sum_trad=sum_old(p,size);
	t1=clock()-t1;
	printf ("%.3f seconds for traditional addition\n", 
          ((double)t1)/CLOCKS_PER_SEC);
	t2=clock();
	sum_rec=sum_new(p,size);
	t2=clock()-t2;
	printf ("%.3f seconds for recursive addition\n", 
          ((double)t2)/CLOCKS_PER_SEC);
	free(p);
	printf("Traditional addition is %d\nRecursive addition is %d\n",sum_trad,sum_rec);
	return 0;
}