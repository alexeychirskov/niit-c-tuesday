/*�������� ���������, ������� ������� ����� �� ���������*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 35//���-�� �����
#define M 35//����� ������
void clean_stdin()
{
	int c;
    do {
		c=getchar();
    }while (c !='\n' && c !=EOF);
}
int labyrinth(char **pmas, int i, int j, int const max_size, const int count)
{
	int static number=0;
	number++;
	if(pmas[i][j]=='#')//�������� �� ��������� � ����� � ������ ���
	{
		if(number==1)
		{
			printf("Wrong coordinates of center!\n");
			exit(1);
		}
		else//��������� � ����� � ��������� ����
			return 0;
	}
	if(pmas[i][j]=='*')
		return 0;
	if(i==0 || j==max_size || i==count || j==0)//������� ���������
	{
		printf("You find the end of labyrinth!");
		printf(" i=%d, j=%d\n",i,j);
		exit(1);
	}
	pmas[i][j]='*';
	labyrinth(pmas,i,j+1,max_size,count);
	labyrinth(pmas,i,j-1,max_size,count);
	labyrinth(pmas,i+1,j,max_size,count);
	labyrinth(pmas,i-1,j,max_size,count);
}
int main()
{
	int i=0,j=0,count=0,size,max_size=0,flag;
	char mas[N][M]={0}, *pmas[N]={0};
	FILE *fp;
	fp=fopen("labyrinth.txt","rt");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	while(fgets(mas[count],M,fp))//��������� ������ ���������
	{
		pmas[count]=mas[count];
		count++;
	}
	for(i=0;i<count;i++)//����� ����� ������� ������ � ���������
	{
		size=strlen(mas[i]);
		if(size>max_size)
			max_size=size;
	}
	//������ ���������� ������
	do{
		flag=1;
		printf("Enter coordinates of center X: ");
		if(scanf("%d",&j)<1)
		{
			flag=0;
			printf("Error! Try again please!\n");
			clean_stdin();
		}
	}while(flag==0);
	do{
		flag=1;
		printf("Enter coordinates of center Y: ");
		if(scanf("%d",&i)<1)
		{
			flag=0;
			printf("Error! Try again please!\n");
			clean_stdin();
		}
	}while(flag==0);
	labyrinth(pmas,i-1,j-1,max_size-2,count-1);
	printf("There is no exit!\n");
	fclose(fp);
	return 0;
}