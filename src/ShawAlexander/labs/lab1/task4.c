/*
4. �������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.
*/
#include <stdio.h>
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
int main()
{
	int feet, inches;
    while (1) {
        puts("Enter values of feet and inches in the following format (10 10):");
        if (scanf("%d%d", &feet, &inches) == 2 && (feet >= 0 && inches >= 0))
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
	double centimeters;
	if (feet != 0)
		centimeters = ((feet * 12.0) + inches) * 2.54;
	else
		centimeters = inches * 2.54;
	printf("%.1f centimeters\n", centimeters);
	return 0;
}