/*
1. �������� ���������, �������������� ���������� ���� �� �����-
��� ������������� ������
*/
#include <stdio.h>
#include <ctype.h>
#define SIZE 2000
int main() {
    char string[SIZE];
	puts("Enter a string:");
    fgets(string, SIZE, stdin);
    short startFound = 0;
    int wordCount = 0;
    for (int i = 0; i < strlen(string); i++) {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
        }
        else if (isspace(c) && startFound == 1) {
            wordCount++;
            startFound = 0;
        }
    }
    printf("Words: %d\n", wordCount);
    return 0;
}