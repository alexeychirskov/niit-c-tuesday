/*
5. �������� ���������, ������� ��������� ������������� ������ �������
N
, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)
*/

#include <stdio.h>
#include <stdlib.h>
#define SIZE 10
int main() {
    srand(time(0));
    short array[SIZE];
    int positives = 0, negatives = 0;
    //���������� ������� ���������� �������
    for (int i = 0; i < SIZE; i++)
    {
        if (positives < SIZE / 2 && negatives < SIZE / 2)
        {
            int coin = rand() % 2;
            if (coin == 0) {
                array[i] = rand();
                positives++;
            }
            else {
                array[i] = -rand();
                negatives++;
            }
        }
        else if (positives >= SIZE / 2)
            array[i] = -rand();
        else if (negatives >= SIZE / 2)
            array[i] = rand();
    }
    int start = -1, end = -1;
    //������� ������ ������������� � ��������� ������������� �����
    for (int i = 0; i < SIZE; i++)
    {
        if (start != -1 && end != -1)
            break;
        else if (start == -1 && array[i] < 0) {
            start = i;
        }
        else if (start != -1 && end == -1) {
            for (int q = SIZE - 1; q > 0; q--)
            {
                if (array[q] > 0) {
                    end = q;
                    break;
                }
            }
        }
    }
    //��������
    for (int i = 0; i < SIZE; i++) {
        printf("%d\n", array[i]);
    }
    int result = 0;
    for (int i = start; i <= end; i++)
    {
        result += array[i];
    }
    printf("Result = %d\n", result);
    return 0;
}