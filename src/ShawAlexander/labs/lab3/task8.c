/*
8. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� N, � ����� ������� N - �� ����� ������ �� �����.
� ������ ������������� N ��������� ��������� �� ������
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 2000
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != 0);
}
int main() {
    char string[SIZE];
    int N = 0;
    puts("Enter a string:");
    fgets(string, SIZE, stdin);
    while (1) {
        puts("Enter the index of a word:");
        if (scanf("%d", &N) == 1)
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
    short startFound = 0, wordPrinted = 0;
    int start = -1, wordCount = 0;

    for (int i = 0; i < strlen(string); i++) {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
            start = i;
        }
        if ((isspace(c) || c == ',' || c == '.') && startFound == 1) {
            wordCount++;
            if (wordCount == N) {
                for (int q = start; q < i; q++) {
                    putchar(string[q]);
                }
                putchar('\n');
                wordPrinted = 1;
            }
            else {
                startFound = 0;
            }
        }
    }
    if (N > wordCount || N <= 0)
        puts("Wrong index!");
    return 0;
}