/*
2. Написать программу ”Угадай число”. Программа задумывает число в диапа-
зоне от 1 до 100 и пользователь должен угадать его за наименьшее количество
попыток.
*/
#include <stdio.h>
#include <stdlib.h>

int main() {
    srand(time(0));
    int number = (rand() % 100) + 1;
    int guess = 0;
    printf("Try to guess a number from 1 to 100:\n");
    while (guess != number) {
        scanf("%d", &guess);
        if (guess == number) {
            printf("Correct!\n");
        }
        else if (guess > number)
            printf("Your number is larger.\n");
        else if (guess < number)
            printf("Your number is smaller.\n");
    }
    return 0;
}