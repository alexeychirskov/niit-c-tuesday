/*
4. �������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define SIZE 25

int main() {
    srand(time(0));
    char string[SIZE+1] = { 0 };
    for (int i = 0; i < SIZE; i++) //��������� ��������� ������
    {
        char c;
        int coin = (rand() % 2);
        if (coin == 1)
            c = (rand() % (10)) + '0';
        else
            c = (rand() % (26)) + 'a';
        string[i] = c;
    }
    printf("BEFORE:\n%s\n", string);
    for (int a = 0, b = SIZE-1; a < b;)
    {
        char lv = string[a], rv = string[b];
        if (isdigit(lv))
            a++;
        if (!isdigit(rv))
            b--;
        if ((!isdigit(lv) && isdigit(rv))) {
            char tmp = lv;
            string[a] = string[b];
            string[b] = tmp;
            a++, b--;
        }
    }
    printf("AFTER:\n%s\n", string);
    return 0;
}